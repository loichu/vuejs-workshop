# Marche à suivre ToDoList

## 1. Installer l'environnement sur Windows
1. Installer [nodejs](https://nodejs.org/dist/v14.16.0/node-v14.16.0-x86.msi). Cocher la case "Installer les outils additionnels recommandés".
1. [Télécharger l'archive du projet](https://gitlab.com/loichu/vuejs-workshop/-/archive/todolist-starter/vuejs-workshop-todolist-starter.zip), l'extraire dans `Documents`.
1. Ouvrir le répertoire dans l'invite de commandes de votre choix et exécuter `npm install` puis `npm run serve`.
1. Ouvrir le répertoire dans VSCode.
1. Ouvrir le fichier `src/App.vue` et accepter d'installer l'extension lorsque VSCode le propose.
2. Ouvrir `Google Chrome` à [http://localhost:8080]() et installer l'[extension Vue Devtools](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd?hl=en).

## 2. Créer le layout (CSS Grids)
1. Créer les composants du layout dans le répertoire `components/`:
```
.
├── ...
├── components
│   ├── TheContent.vue
│   ├── TheHeader.vue
│   └── TheSidebar.vue
└── ...
```
2. Aller dans chacun de ces composants et générer le code de base à l'aide du snippet `<vue TAB`. Régler `vscode` pour qu'il s'adapte à l'indentation du code généré. Créer les éléments racine et nommer les composants afin qu'ils ressemblent finalement à ça:
```vue
<template>
  <div id="content"></div>
</template>

<script>
export default {
  name: "TheContent",
}
</script>

<style scoped>

</style>
```
3. Importer les composants et créer la grille CSS dans `src/App.vue`:
```vue
<template>
  <div id="app">
    <TheHeader />
    <TheSidebar />
    <TheContent />
  </div>
</template>

<script>
// Components
import TheHeader from './components/TheHeader'
import TheSidebar from './components/TheSidebar'
import TheContent from './components/TheContent'

export default {
  name: 'App',
  components: {
    TheHeader,
    TheSidebar,
    TheContent
  },
}
</script>

<style>
#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  color: #2c3e50;

  /* Take full viewport */
  width: 100vw;
  height: 100vh;

  /* CSS grid */
  display: grid;
  grid-template-areas:
    "header  header"
    "sidebar main";
}
</style>
```
4. Isoler le style de **chaque composant**, l'inscrire dans la grille et lui donner une couleur afin de le visualiser:
```vue
<style scoped>
#header {
  background-color: lightgrey;

  /* Grid */
  grid-area: header;
}
</style>
```
5. Ajuster les proportions de la grille dans `src/App.vue` afin de correspondre au layout attendu:
```vue
<style>
body {
  margin: 0;
}

#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  color: #2c3e50;

  /* Take full viewport */
  width: 100vw;
  height: 100vh;

  /* CSS grid */
  display: grid;
  grid-template-rows: 70px 1fr;
  grid-template-columns: 250px 1fr;
  grid-template-areas:
    "header  header"
    "sidebar main";
}
</style>
```

## 3. Créer le header (en-tête)
Nous allons créer une en-tête à trois colonnes: le nom du site, une barre de recherche et une salutation. Le nom du site et le nom de la personne à saluer seront définis dans `App` mais affichés dans `TheHeader` grâce aux **propriétés (props)**.

1. Créer le `template` du header:
```vue
<template>
  <header id="header">
    <div class="side">{{ appName }}</div>

    <div>
      <form>
        <input type="search">
        <input type="submit" value="Search">
      </form>
    </div>

    <div class="side">Hello {{ userName }} !</div>
  </header>
</template>
```
2. Créer les **propriétés (props)**:
```vue
<script>
export default {
  name: "TheHeader",
  props: {
    appName: String,
    userName: String
  },
}
</script>
```
3. Les définir dans `App`:
```vue
<template>
  <div id="app">
    <TheHeader appName="Todo List" userName="Loïc" />
    [...]
  </div>
</template>
```
4. Aligner l'en-tête avec un peu de style en utilisant `flexbox` dans `TheHeader`:
```vue
<style scoped>
#header {
  background-color: lightgrey;
  padding: 20px;
  font-size: 1.5rem;

  /* Grid */
  grid-area: header;

  /* Flexbox */
  display: flex;
  justify-content: space-between;
  align-items: center;
}

.side {
  width: 150px;
}
</style>
```
5. Nous reviendrons sur la barre de recherche plus tard.

## 4. Importer les todos
1. Dans `src/App.vue`, importer le fichier `src/data/todos.json`. Ce seront des **données (data)** pour `App`. Les passer comme **propriétés (props)** à `TheContent`:
```vue
<template>
  <div id="app">
    [...]
    <TheContent :todos="todos" />
  </div>
</template>

<script>
//[...]
// Data
import todos from './data/todos'

export default {
  //[...]
  data() {
    return {
      todos
    }
  }
}
</script>
```
2. Dans `src/components/TheContent.vue`, déclarer la propriété `todos`:
```vue
<script>
export default {
  name: "TheContent",
  props: {
    todos: Array
  }
}
</script>
```

## 5. Afficher les todos
1. Nous allons créer un nouveau composant dont le rôle sera d'afficher un `todo`. Pour ce faire, nous allons créer un nouveau fichier `src/components/TodoItem.vue`:
```vue
<template>
  <div class="todo-item">
    <h3>{{ title }}</h3>
    <p>{{ description }}</p>
  </div>
</template>

<script>
export default {
  name: "TodoItem",
  props: {
    title: String,
    description: String
  }
}
</script>
```
2. **Chaque todo** sera afficher dans ce composant grâce à une boucle **`v-for`** dans `src/components/TheContent.vue`. Nous utilisons **`v-bind`** pour donner chaque propriété de l'**objet** `todo` comme propriété au **composant** `TodoItem`:
```vue
<template>
  <div id="content">
    <TodoItem v-for="todo in todos" :key="todo.id" v-bind="todo" />
  </div>
</template>
```

## 6. La sidebar (menu latérale)
La sidebar nous permet d'afficher et de sélectionner les listes de todos. Dans le fichier `src/data/lists.json` nous avons la liste intitulée "maison" qui contient nos cinq todos initiale et une autre intitulée "divers" qui n'en contient aucune. `TheContent` doit afficher uniquement le contenu de la liste sélectionnée.

1. Importer les listes dans `App` et les donner comme propriété à `TheSidebar`:
```vue
<template>
  <div id="app">
    [...]
    <TheSidebar :lists="lists" />
    [...]
  </div>
</template>

<script>
//[...]
import lists from './data/lists'

export default {
  //[...]
  data() {
    return {
      todos,
      lists
    }
  }
}
</script>
```
2. Dans `TheSidebar`, afficher chaque liste dans un `div` et récupérer l'**évènement** lorsque l'on clique sur l'un d'eux:
```vue
<template>
  <div id="sidebar">
    <div v-for="list in lists" :key="list.id" class="list-item"
      @click="selectList(list.id)">
      {{ list.name }}
    </div>
  </div>
</template>

<script>
export default {
  name: "TheSidebar",
  props: {
    lists: Array
  },
  methods: {
    selectList( listId ) {
      console.log(listId)
    }
  }
}
</script>
```
3. Renvoyer un **évènement personnalisé** avec l'`id` de la liste sélectionnées:
```vue
<script>
export default {
  //[...]
  methods: {
    selectList( listId ) {
      this.$emit("select-list", listId)
    }
  }
}
</script>
```
4. Ajouter une nouvelle **donnée (data)** dans `App` pour stocker la liste sélectionnée:
```vue
<script>
  //[...]
  data() {
    return {
      todos,
      lists,
      activeList: 1
    }
  },
  //[...]
</script>
```
5. Récupérer l'évènement personnalisé dans `App` et mettre à jour `activeList`:
```vue
<template>
  <div id="app">
    [...]
    <TheSidebar :lists="lists" @select-list="selectList" />
    [...]
  </div>
</template>

<script>
  //[...]
  methods: {
    selectList( listId ) {
      this.activeList = listId
    }
  },
  //[...]
</script>
```
6. Transformer la **donnée `todos`** en **propriété calculée (`computed`)** afin de pouvoir **filtrer** les todos:
```vue
<script>
  //[...]
  data() {
    return {
      lists,
      activeList: 1
    }
  },
  //[...]
  computed: {
    todos() {
      const activeList = this.activeList
      return todos.filter(
        todo => todo.listId === activeList
      )
    }
  }
</script>
```
## 7. La barre de recherche (BONUS)
Sachant tout ça, seriez-vous capable d'implémenter la fonctionnalité de la recherche ?