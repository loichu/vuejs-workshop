# VueJS-Workshop
Une introduction aux applications par composants avec VueJS. Faite pour les pré-apprentis de l'ETML.

Le but est de présenter le sujet par la pratique selon la technique "[workshop](https://unsiap.or.jp/tnetwork/1703_TOT/WorkshopManual.pdf)".

## Table des matières
1. [Généralités](#vuejs-workshop)
1. [Support de cours](#support-de-cours)
2. [Marche à suivre ToDoList](#marche-à-suivre-todolist)

## Objectifs
Les objectifs sont triés selon la taxonomie de Blooms. À part pour les objectifs entre parenthèses, un objectif de taxonomie plus élevée nécessite l'acquisition (au moins partielle) des objectifs de taxonomie inférieure. Afin d'assurer l'acquisition maximale de ces objectifs, des allés-retours dans les taxonomies sont effectués avec, à chaque fois, des méthodes et/ou techniques différentes. Chaque tâche vise une ou deux taxonomies en particulier, le détail sur les objectifs visés se trouve dans les tâches détaillées. Ceux-ci sont encore plus détaillés dans le détail de la marche à suivre pas à pas.

![La rose de Blooms](doc/img/Blooms_rose_fr.svg)

### 1. Connaissance
* Identifier les différents acteurs d'une application en VueJS;
* Définir ce qu'est un composant d'interface utilisateur.

### 2. Compréhension
* Différencier les différents acteurs d'une application en VueJS;
* Expliquer la différence entre une propriété et une donnée dans un composant VueJS;
* Identifier les évènements qui peuvent être générés par un utilisateur.

### 3. Application
* (Installer un environnement);
* (Importer un projet existant);
* Développer à l'aide du langage JavaScript et son écosystème;
* Représenter son interface web en petits composants réutilisables;
* Utiliser des données JSON pour construire son interface web;
* Interpréter un évènement généré par l'utilisateur;
* Employer un évènement personnalisé;
* Appliquer un changement dans les données à afficher.

### 4. Analyse
* Transposer une application web VueJS dans un contexte différent.

### Bonus:
* (Synthèse) Modifier la propriété `computed` qui contient les données à afficher afin d'implémenter la fonctionnalité de recherche;
* (Analyse) Générer le site static et le mettre en ligne sur une `gitlab page`.

## Planning

Les quatres méthodes qui sont utilisées sont: démonstrative, interrogative, active, expositive. Étant donner qu'il s'agit ici d'un workshop, la méthode active est favorisée, les autres sont des supports pour avoir les ressources nécessaires à la pratique.

|   Horaire      |           Tâche                      |      Méthode(s)           | Taxonomie(s) (c.f. objectifs)               |
|----------------|--------------------------------------|---------------------------|---------------------------------------------|
| 08h00 → 08h20  | Intro (composants, JS et écosystème) | Expositive, interrogative | 1. Connaissance                             |
| 08h20 → 08h50  | Installer l'environnement            | Démonstrative, active     | (3) Importer un projet existant             |
| 08h50 → 09h10  | Expliquer l'environnement            | Expositive, démonstrative | 2. Compréhension                            |
| 09h10 → 10h00  | Développer la ToDoList pas à pas     | Démonstrative, active     | 3. Application                              |
| 10h00 → 10h15  | Pause                                | ∅                         | ∅                                           |
| 10h15 → 10h35  | Récapitulation                       | Interrogative             | 1,2. Connaissances + compréhension          |
| 10h35 → 10h50  | Créer l'application Playlists        | Active                    | 3,4. Application + analyse                  |
| 10h50 → 11h00  | Point sur les acquis                 | Interrogative             | 1,2. Connaissances + compréhension          |
| 11h00 → 11h25  | Finalisation de l'application        | Active                    | 3,4,(5). Application + analyse (+ synthèse) |

## Tâches détaillées

### 1. Intro (composants, JS et écosystème)

#### Objectifs
* Identifier les différents acteurs d'une application en VueJS;
* Définir ce qu'est un composant d'interface utilisateur.

#### Rôle de l'enseignant
Présenter le programme de la matinée. Contextualiser le cours à l'aide d'une présentation dynamique au projecteur. Répondre aux questions et s'aidant éventuellement du tableau noir.

#### Rôle de l'élève
Prendre connaissance, s'intéresser, poser des questions.

### 2. Installer l'environnement

#### Objectifs
* Installer un environnement;
* Importer un projet existant.

#### Rôle de l'enseignant
Installer l'environnement en même temps que les élèves en expliquant chaque étape décrite au [point 1 de la marche à suivre](https://gitlab.com/loichu/vuejs-workshop#1-installer-lenvironnement-sur-windows).

#### Rôle de l'élève
Installer l'environnement en suivant les étapes de l'enseignant. Comprendre ce qu'on est en train de faire.

### 3. Expliquer l'environnement

#### Objectif
* Différencier les différents acteurs d'une application en VueJS.

#### Rôle de l'enseignant
Ouvrir chaque fichier dans `vscode` et expliquer son contenu et son rôle. Remontrer les slides en rapport. Répondre aux questions.

#### Rôle de l'élève
Ouvrir chaque fichier en même temps que l'enseignant. Comprendre son contenu et son rôle. Poser des questions en cas de doute ou d'incompréhension.

### 4. Développer la ToDoList pas à pas

#### Objectifs
* Identifier les évènements qui peuvent être générés par un utilisateur.
* Développer à l'aide du langage JavaScript et son écosystème;
* Représenter son interface web en petits composants réutilisables;
* Utiliser des données JSON pour construire son interface web;
* Interpréter un évènement généré par l'utilisateur;
* Employer un évènement personnalisé;
* Appliquer un changement dans les données à afficher.

#### Rôle de l'enseignant
Guider les élèves selon [la marche à suivre pas à pas](https://gitlab.com/loichu/vuejs-workshop#marche-%C3%A0-suivre-todolist). Apporter les connaissances théoriques au fur et à mesure qu'elles sont utiles dans l'application. Répondre aux questions.

#### Rôle de l'élève
Suivre la marche à suivre donnée par l'enseignant. Comprendre chaque aspect de l'application. Poser des questions.

### 5. Récapitulation

#### Objectifs
* Identifier les différents acteurs d'une application en VueJS;
* Définir ce qu'est un composant d'interface utilisateur;
* Différencier les différents acteurs d'une application en VueJS;
* Expliquer la différence entre une propriété et une donnée dans un composant VueJS;
* Identifier les évènements qui peuvent être générés par un utilisateur.

#### Rôle de l'enseignant
Remontrer certaines slides et demander aux élèves de l'expliquer avec leurs mots. Apporter des corrections et ajouter des détails importants. Tenter de faire participer tous les élèves.

#### Rôle de l'élève
Expliquer les slides importantes. S'assurer d'avoir bien compris.

### 6. Créer l'application Playlists

#### Objectifs
* Importer un projet existant;
* Développer à l'aide du langage JavaScript et son écosystème;
* Représenter son interface web en petits composants réutilisables;
* Utiliser des données JSON pour construire son interface web;
* Interpréter un évènement généré par l'utilisateur;
* Employer un évènement personnalisé;
* Appliquer un changement dans les données à afficher;
* Transposer une application web VueJS dans un contexte différent.

#### Rôle de l'enseignant
Expliquer que cette application ressemble beaucoup à celle réalisée précédemment en mode pas à pas, les étapes seront les mêmes. Montrer à quoi doit ressembler le résultat final. Montrer où trouver les ressources nécessaires c'est-à-dire: les présentations montrées précédemment, quelques liens vers des documentations de référence ainsi qu'un projet de base sur lequel commencer à travailler. Rester disponible pour les demandes d'aides et les questions. Passer vers chaque élève pour s'assurer qu'il ne soit pas bloqué.

#### Rôle de l'élève
Se servir du projet `ToDoList` et des ressources mises à disposition pour réaliser l'application `Playlists` en suivant les mêmes étapes que pour le premier projet. Demander de l'aide et poser des questions en cas de besoin.

### 7. Point sur les acquis

#### Objectifs
**Tous les objectifs**

#### Rôle de l'enseignant
Évaluer les acquis des élèves en leur posant des questions par oral et en leur demandant d'identifier les points clés ou les difficultés particulières. Faire participer tous les élèves. Poser des questions plus ou moins complexes à chaque élève selon sa progression personnelle. Si le niveau est suffisamment élevé, proposer d'ajouter la fonctionnalité de la barre de recherche et/ou de se renseigner sur comment le publier une `gitlab page`.

#### Rôle de l'élève
Répondre aux questions qui lui sont posées. Se situer par rapports aux objectifs du cours. Se rendre compte de tout ce qu'il a appris.

### 8. Finalisation de l'application
Si l'enseignant est toujours présent, continuer d'aider comme à la [tâche 6](https://gitlab.com/loichu/vuejs-workshop#6-cr%C3%A9er-lapplication-playlists). Sinon les élèves continuent à apprendre et progresser de manière autonome.

# Support de cours
Le support de cours est constitué de [ce dépôt](https://gitlab.com/loichu/vuejs-workshop) pour la partie pratique ainsi que de [cette présentation](https://loichu.gitlab.io/vuejs-workshop) pour la partie théorique.

# Marche à suivre ToDoList

## 1. Installer l'environnement sur Windows
Objectifs:
* Installer un environnement;
* Importer un projet existant.

> :warning: `vue-cli` ne fonctionne pas sur les ordinateurs de l'école.
Il faut donc utiliser la [méthode docker](#1-alternative-installer-sur-docker) si vous souhaitez l'utiliser. Sinon continuer **sans `vue-cli`**.

1. Installer [nodejs](https://nodejs.org/dist/v14.16.0/node-v14.16.0-x86.msi). Cocher la case "Installer les outils additionnels recommandés".
1. [Télécharger l'archive du projet](https://gitlab.com/loichu/vuejs-workshop/-/archive/todolist-starter/vuejs-workshop-todolist-starter.zip), l'extraire dans `Documents`.
1. Ouvrir le répertoire dans l'invite de commandes de votre choix et exécuter `npm install` puis `npm run serve`.
1. Ouvrir le répertoire dans VSCode.
1. Ouvrir le fichier `src/App.vue` et accepter d'installer l'extension lorsque VSCode le propose.
1. Ouvrir `Google Chrome` à [http://localhost:8080]() et installer l'[extension Vue Devtools](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd?hl=en).

## 1. (Alternative) Installer sur Docker :warning: TODO
> Si Docker indique une erreur de virtualisation, c'est probablement que Hyper-V est désactivé. Pour l'activer, lancer `bcdedit /set hypervisorlaunchtype auto` puis `dism /Online /Enable-Feature:Microsoft-Hyper-V /All` dans un cmd administrateur puis redémarrer.

1. Vérifier que docker est installé en cherchant `Docker` dans la barre de recherche de Windows.
1. Télécharger [le projet](/) puis extraire l'archive dans le répertoire `Documents`.

## 2. Créer le layout (CSS Grids)
Objectifs:
* Développer à l'aide du langage JavaScript et son écosystème;
* Représenter son interface web en petits composants réutilisables.

1. Créer les composants du layout dans le répertoire `components/`:
```
.
├── ...
├── components
│   ├── TheContent.vue
│   ├── TheHeader.vue
│   └── TheSidebar.vue
└── ...
```
2. Aller dans chacun de ces composants et générer le code de base à l'aide du snippet `<vue TAB`. Régler `vscode` pour qu'il s'adapte à l'indentation du code généré. Créer les éléments racine et nommer les composants afin qu'ils ressemblent finalement à ça:
```vue
<template>
  <div id="content"></div>
</template>

<script>
export default {
  name: "TheContent",
}
</script>

<style scoped>

</style>
```
3. Importer les composants et créer la grille CSS dans `src/App.vue`:
```vue
<template>
  <div id="app">
    <TheHeader />
    <TheSidebar />
    <TheContent />
  </div>
</template>

<script>
// Components
import TheHeader from './components/TheHeader'
import TheSidebar from './components/TheSidebar'
import TheContent from './components/TheContent'

export default {
  name: 'App',
  components: {
    TheHeader,
    TheSidebar,
    TheContent
  },
}
</script>

<style>
#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  color: #2c3e50;

  /* Take full viewport */
  width: 100vw;
  height: 100vh;

  /* CSS grid */
  display: grid;
  grid-template-areas:
    "header  header"
    "sidebar main";
}
</style>
```
4. Isoler le style de **chaque composant**, l'inscrire dans la grille et lui donner une couleur afin de le visualiser:
```vue
<style scoped>
#header {
  background-color: lightgrey;

  /* Grid */
  grid-area: header;
}
</style>
```
5. Ajuster les proportions de la grille dans `src/App.vue` afin de correspondre au layout attendu:
```vue
<style>
body {
  margin: 0;
}

#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  color: #2c3e50;

  /* Take full viewport */
  width: 100vw;
  height: 100vh;

  /* CSS grid */
  display: grid;
  grid-template-rows: 70px 1fr;
  grid-template-columns: 250px 1fr;
  grid-template-areas:
    "header  header"
    "sidebar main";
}
</style>
```
## 3. Créer le header (en-tête)
Objectifs:
* **Représenter son interface web en petits composants réutilisables;**
* Développer à l'aide du langage JavaScript et son écosystème;
* Expliquer la différence entre une propriété et une donnée dans un composant VueJS.

Nous allons créer une en-tête à trois colonnes: le nom du site, une barre de recherche et une salutation. Le nom du site et le nom de la personne à saluer seront définis dans `App` mais affichés dans `TheHeader` grâce aux **propriétés (props)**.

1. Créer le `template` du header:
```vue
<template>
  <header id="header">
    <div class="side">{{ appName }}</div>

    <div>
      <form>
        <input type="search">
        <input type="submit" value="Search">
      </form>
    </div>

    <div class="side">Hello {{ userName }} !</div>
  </header>
</template>
```
2. Créer les **propriétés (props)**:
```vue
<script>
export default {
  name: "TheHeader",
  props: {
    appName: String,
    userName: String
  },
}
</script>
```
3. Les définir dans `App`:
```vue
<template>
  <div id="app">
    <TheHeader appName="Todo List" userName="Loïc" />
    [...]
  </div>
</template>
```
4. Aligner l'en-tête avec un peu de style en utilisant `flexbox` dans `TheHeader`:
```vue
<style scoped>
#header {
  background-color: lightgrey;
  padding: 20px;
  font-size: 1.5rem;

  /* Grid */
  grid-area: header;

  /* Flexbox */
  display: flex;
  justify-content: space-between;
  align-items: center;
}

.side {
  width: 150px;
}
</style>
```
5. Nous reviendrons sur la barre de recherche plus tard.
## 4. Importer les todos
Objectifs:
* **Utiliser des données JSON pour construire son interface web;**
* Développer à l'aide du langage JavaScript et son écosystème;
* Expliquer la différence entre une propriété et une donnée dans un composant VueJS.

1. Dans `src/App.vue`, importer le fichier `src/data/todos.json`. Ce seront des **données (data)** pour `App`. Les passer comme **propriétés (props)** à `TheContent`:
```vue
<template>
  <div id="app">
    [...]
    <TheContent :todos="todos" />
  </div>
</template>

<script>
//[...]
// Data
import todos from './data/todos'

export default {
  //[...]
  data() {
    return {
      todos
    }
  }
}
</script>
```
2. Dans `src/components/TheContent.vue`, déclarer la propriété `todos`:
```vue
<script>
export default {
  name: "TheContent",
  props: {
    todos: Array
  }
}
</script>
```
## 5. Afficher les todos
Objectifs:
* **Représenter son interface web en petits composants réutilisables;**
* Développer à l'aide du langage JavaScript et son écosystème;
* Utiliser des données JSON pour construire son interface web;
* Expliquer la différence entre une propriété et une donnée dans un composant VueJS.

1. Nous allons créer un nouveau composant dont le rôle sera d'afficher un `todo`. Pour ce faire, nous allons créer un nouveau fichier `src/components/TodoItem.vue`:
```vue
<template>
  <div class="todo-item">
    <h3>{{ title }}</h3>
    <p>{{ description }}</p>
  </div>
</template>

<script>
export default {
  name: "TodoItem",
  props: {
    title: String,
    description: String
  }
}
</script>
```
2. **Chaque todo** sera afficher dans ce composant grâce à une boucle **`v-for`** dans `src/components/TheContent.vue`. Nous utilisons **`v-bind`** pour donner chaque propriété de l'**objet** `todo` comme propriété au **composant** `TodoItem`:
```vue
<template>
  <div id="content">
    <TodoItem v-for="todo in todos" :key="todo.id" v-bind="todo" />
  </div>
</template>
```
## 6. La sidebar (menu latérale)
Objectifs:
* Identifier les évènements qui peuvent être générés par un utilisateur.
* Interpréter un évènement généré par l'utilisateur;
* Employer un évènement personnalisé;
* Appliquer un changement dans les données à afficher.

La sidebar nous permet d'afficher et de sélectionner les listes de todos. Dans le fichier `src/data/lists.json` nous avons la liste intitulée "maison" qui contient nos cinq todos initiale et une autre intitulée "divers" qui n'en contient aucune. `TheContent` doit afficher uniquement le contenu de la liste sélectionnée.

1. Importer les listes dans `App` et les donner comme propriété à `TheSidebar`:
```vue
<template>
  <div id="app">
    [...]
    <TheSidebar :lists="lists" />
    [...]
  </div>
</template>

<script>
//[...]
import lists from './data/lists'

export default {
  //[...]
  data() {
    return {
      todos,
      lists
    }
  }
}
</script>
```
2. Dans `TheSidebar`, afficher chaque liste dans un `div` et récupérer l'**évènement** lorsque l'on clique sur l'un d'eux:
```vue
<template>
  <div id="sidebar">
    <div v-for="list in lists" :key="list.id" class="list-item"
      @click="selectList(list.id)">
      {{ list.name }}
    </div>
  </div>
</template>

<script>
export default {
  name: "TheSidebar",
  props: {
    lists: Array
  },
  methods: {
    selectList( listId ) {
      console.log(listId)
    }
  }
}
</script>
```
3. Renvoyer un **évènement personnalisé** avec l'`id` de la liste sélectionnées:
```vue
<script>
export default {
  //[...]
  methods: {
    selectList( listId ) {
      this.$emit("select-list", listId)
    }
  }
}
</script>
```
4. Ajouter une nouvelle **donnée (data)** dans `App` pour stocker la liste sélectionnée:
```vue
<script>
  //[...]
  data() {
    return {
      todos,
      lists,
      activeList: 1
    }
  },
  //[...]
</script>
```
5. Récupérer l'évènement personnalisé dans `App` et mettre à jour `activeList`:
```vue
<template>
  <div id="app">
    [...]
    <TheSidebar :lists="lists" @select-list="selectList" />
    [...]
  </div>
</template>

<script>
  //[...]
  methods: {
    selectList( listId ) {
      this.activeList = listId
    }
  },
  //[...]
</script>
```
6. Transformer la **donnée `todos`** en **propriété calculée (`computed`)** afin de pouvoir **filtrer** les todos:
```vue
<script>
  //[...]
  data() {
    return {
      lists,
      activeList: 1
    }
  },
  //[...]
  computed: {
    todos() {
      const activeList = this.activeList
      return todos.filter(
        todo => todo.listId === activeList
      )
    }
  }
</script>
```
## 7. La barre de recherche (BONUS)
Sachant tout ça, seriez-vous capable d'implémenter la fonctionnalité de la recherche ?
