# VueJS-Workshop

Une introduction aux applications par composants avec VueJS. Faite pour les pré-apprentis de l'ETML.

Le but est de présenter le sujet par la pratique selon la méthode "[workshop](https://unsiap.or.jp/tnetwork/1703_TOT/WorkshopManual.pdf)".

## 1. Installer l'environnement sur Windows
> :warning: `vue-cli` ne fonctionne pas sur les ordinateurs de l'école.
Il faut donc utiliser la [méthode docker](#1-alternative-installer-sur-docker).

1. Installer [nodejs](https://nodejs.org/dist/v14.16.0/node-v14.16.0-x86.msi). Cocher la case "Installer les outils additionnels recommandés".
1. Créer un répertoire (dossier) dans `Documents` qui s'appelle `vuejs-workshop` et s'y rendre dans PowerShell. Garder la fenêtre ouverte.
1. Installer `vue-cli` à l'aide de la commande `npm install -g @vue/cli` dans PowerShell.

## 1. (Alternative) Installer sur Docker
> Si Docker indique une erreur de virtualisation, c'est probalblement que Hyper-V est désactivé. Pour l'activer, lancer `bcdedit /set hypervisorlaunchtype auto` puis `dism /Online /Enable-Feature:Microsoft-Hyper-V /All` dans un cmd administrateur puis redémarrer.

1. Vérifier que docker est installé en cherchant `Docker` dans la barre de recherche de Windows.
1. Télécharger [le projet](/) puis extraire l'archive dans le répertoire `Documents`.
